# Simple Website with Vanilla JS

## Project Objectives

This project aims to create a simple website using Vanilla JavaScript. It consists of two tasks:

## Task 1 - Bootstrap Modal and Slider

### Overview

In this task, we'll utilize Bootstrap to enhance our website by adding a modal and a slider. The modal should appear when users click on the pricing options. It will prompt them to provide their name, email, and order comments. Additionally, we'll implement a slider to highlight different pricing plans based on the number of users.

## Task 2 - Form Submission and Core Web Vitals

### Overview

For this task, we'll sign up on [https://forms.maakeetoo.com](https://forms.maakeetoo.com) and populate the form submission with the contents gathered from Task 1. We'll also evaluate the website's Core Web Vitals by using the Lighthouse tool in the developer's console. The objective is to optimize Core Web Vitals for both desktop and mobile platforms.

